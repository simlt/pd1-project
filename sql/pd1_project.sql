-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Giu 08, 2016 alle 21:18
-- Versione del server: 5.5.49-0ubuntu0.14.04.1
-- Versione PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pd1_project`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `BOOKING`
--

DROP TABLE IF EXISTS `BOOKING`;
CREATE TABLE IF NOT EXISTS `BOOKING` (
  `BID` int(11) NOT NULL AUTO_INCREMENT,
  `MID` int(11) NOT NULL,
  `start` time NOT NULL,
  `duration` int(11) NOT NULL,
  `ownerUID` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BID`),
  UNIQUE KEY `booking` (`MID`,`start`),
  KEY `ownerUID` (`ownerUID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dump dei dati per la tabella `BOOKING`
--

INSERT INTO `BOOKING` (`BID`, `MID`, `start`, `duration`, `ownerUID`, `created`) VALUES
(1, 1, '12:00:00', 10, 1, '2016-06-08 17:40:03'),
(2, 2, '12:15:00', 30, 1, '2016-06-08 17:40:15'),
(3, 1, '10:00:00', 20, 2, '2016-06-08 17:40:42'),
(4, 2, '10:30:00', 15, 2, '2016-06-08 17:40:51'),
(5, 1, '19:00:00', 15, 3, '2016-06-08 17:41:28'),
(6, 2, '19:30:00', 15, 3, '2016-06-08 17:41:38'),
(7, 1, '21:00:00', 20, 1, '2016-06-08 19:00:31'),
(8, 2, '21:10:00', 30, 2, '2016-06-08 19:00:51');

-- --------------------------------------------------------

--
-- Struttura della tabella `USER`
--

DROP TABLE IF EXISTS `USER`;
CREATE TABLE IF NOT EXISTS `USER` (
  `UID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(320) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `hash` varchar(255) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `USER`
--

INSERT INTO `USER` (`UID`, `email`, `name`, `surname`, `hash`) VALUES
(1, 'u1@p.it', 'Utente', '1', '$2y$10$iLOycpdzwEoc1cAQHK37jeAHel0Vu4hBr1QkxCET551OuBUPnO6/C'),
(2, 'u2@p.it', 'Utente', '2', '$2y$10$rvbji.DAZypVAdx0hTFu0exvPatGHn5sAgOBz6/EfZETo245lgW1.'),
(3, 'u3@p.it', 'Utente', '3', '$2y$10$ligqZo/A4AftDNmZ84OWAOIGwtQOIUIya2z8cUeZnaU4XVOhQ4n12');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `BOOKING`
--
ALTER TABLE `BOOKING`
  ADD CONSTRAINT `BOOKING_ibfk_1` FOREIGN KEY (`ownerUID`) REFERENCES `USER` (`UID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
