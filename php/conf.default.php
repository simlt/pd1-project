<?php

// Setup: Copy this file to conf.php and modify the settings

$config = array(
    "db" => array(
        "dbname" => "db",
        "username" => "root",
        "password" => "password",
        "host" => "localhost"
    ),
    "machines" => 4,
    "inactivityTimeout" => 2*60, // seconds
);


defined("LIB_PATH")
    or define("LIB_PATH", dirname(__FILE__) . '/lib');
defined("TEMPLATE_PATH")
    or define("TEMPLATE_PATH", dirname(__FILE__) . '/template');


/*
    Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);

?>
