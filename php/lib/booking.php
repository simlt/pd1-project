<?php

require_once "functions.php";


function getBookingTable() {
  global $msg;
  if (!$conn = dbConnect())
    return;
  $sql = "SELECT TIME_FORMAT(start, '%H:%i') AS start, duration, MID
    FROM BOOKING B
    ORDER BY start";

  $table = "<table class=\"table table-striped table-bordered\"><tr>
    <th>Inizio</th>
    <th>Durata</th>
    <th>Macchina</th>
    </tr>";

  if (!$res = mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Query non valida: " . mysqli_error($conn);
    mysqli_close($conn);
    return;
  }
  if (mysqli_num_rows($res) === 0) {
    $table = "<h3>Nessuna prenotazione inserita</h3>";
  } else {
    while ($row = mysqli_fetch_assoc($res)) {
      $table = $table . "
        <tr><td>".htmlspecialchars($row["start"])."</td>
        <td>".intval($row["duration"])."</td>
        <td>Macchina ".intval($row["MID"])."</td></tr>";
    }
    $table = $table . "</table>";
  }
  mysqli_free_result($res);
  mysqli_close($conn);
  return $table;
}


function getBookingTableForUser($UID) {
  global $msg;
  if (!$conn = dbConnect())
    return;
  // UID is retrieved from server side session, no need to sanitize
  $UID = intval($UID);
  $sql = "SELECT BID, TIME_FORMAT(start, '%H:%i') AS start, duration, MID, (NOW() - created) > 60 AS old
    FROM BOOKING
    WHERE ownerUID = $UID
    ORDER BY start";

  $table = "<table class=\"table table-striped table-bordered\"><tr>
    <th class=\"tdcheckbox\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></th>
    <th>Inizio</th>
    <th>Durata</th>
    <th>Macchina</th>
    </tr>";

  if (!$res = mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Query non valida: " . mysqli_error($conn);
    mysqli_close($conn);
    return;
  }
  if (mysqli_num_rows($res) === 0) {
    $table = "<h3>Nessuna prenotazione inserita</h3>";
  } else {
    while ($row = mysqli_fetch_assoc($res)) {
      $table = $table . "<tr>";
      // Add checkbox only if older than 1 min
      if ($row["old"])
        $table = $table . "<td><input name=\"remove[]\" type=\"checkbox\" value=".intval($row["BID"])."></td>";
      else
        $table = $table . "<td></td>";

      $table = $table . "<td>".htmlspecialchars($row["start"])."</td>
        <td>".intval($row["duration"])."</td>
        <td>Macchina ".intval($row["MID"])."</td>
        </tr>";
    }
    $table = $table . "</table>
    <button type=\"submit\" class=\"btn btn-danger\">Rimuovi</button>";
  }
  mysqli_free_result($res);
  mysqli_close($conn);
  return $table;
}

// Return TRUE is success
// TODO must also check 1 minute constraint?
function removeBookingsForUser($UID, $BIDs)
{
  global $msg;
  if (!$conn = dbConnect())
    return FALSE;
  // Sanitize
  $UID = intval($UID);
  // Always make sure the user is owner of the booking and check 1 minute age limitation for delete
  $sql = "DELETE FROM BOOKING WHERE (NOW() - created) > 60 AND (ownerUID, BID) IN (";
  for ($i = 0, $l = count($BIDs); $i < $l; $i++) {
    $BID = intval($BIDs[$i]); // no problem here for unexpected data
    if ($i > 0)
      $sql = $sql . ",";
    $sql = $sql . "('$UID', '$BID')";
  }
  $sql = $sql . ")";
  if (!mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Errore durante la rimozione: " . mysqli_error($conn);
    mysqli_close($conn);
    return FALSE;
  }
  mysqli_close($conn);
  $msg = "Prenotazioni rimosse con successo.";
  return TRUE;
}

// Return TRUE is success
function addBookingForUser($UID, $time, $duration) {
  global $msg;
  global $config;
  $MID = -1;
  // Validation
  if ((sscanf($time, "%d:%d", $HH, $MM) != 2) ||
    (filter_var($HH, FILTER_VALIDATE_INT, array("options" => array("min_range" => 0, "max_range" => 23))) === FALSE) ||
    (filter_var($MM, FILTER_VALIDATE_INT, array("options" => array("min_range" => 0, "max_range" => 59))) === FALSE))
  {
    $msg = "Orario di inizio non valido.";
    return FALSE;
  }
  if (filter_var($duration, FILTER_VALIDATE_INT, array("options" => array("min_range" => 0, "max_range" => 60*24))) === FALSE)
  {
    $msg = "Durata non valida.";
    return FALSE;
  }

  if (!$conn = dbConnect())
    return FALSE;
  // Sanitize
  $UID = intval($UID);
  $duration = intval($duration);
  $starttime = mysqli_real_escape_string($conn, $HH.":".$MM); // Escape should not be needed here, but do it for safety

  // Get available machines (TODO what if machines config rises without having enough machines in DB?)
  // TODO check also if 23:59 + 10 min should overlap with 00:00 again (probably not)


  // Get list of unavailable machines SORTED
  $sql = "SELECT MID
    FROM BOOKING
    WHERE ADDTIME(start, SEC_TO_TIME(duration * 60)) > TIME('$starttime')
      AND start < ADDTIME(TIME('$starttime'), SEC_TO_TIME('$duration' * 60))
    GROUP BY MID
    ORDER BY MID";
  if (!$res = mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Query non valida: " . mysqli_error($conn);
    mysqli_close($conn);
    return FALSE;
  }
  // Search for free machine in time slot
  // I.E. find first free ID from 1 to N
  $MID = 0;
  for ($i = 1; $i <= $config["machines"]; $i++) {
    $row = mysqli_fetch_row($res);
    if ($row && ($i == $row[0])) {
      continue; // Not available machine
    }
    // Otherwise $i is free
    $MID = $i;
    break;
  }
  mysqli_free_result($res);

  // $MID = 1; // test multiple concurrent insertion race condition
  if (!$MID) {
    $msg = "Impossibile inserire la prenotazione: nessuna macchina disponibile!";
    mysqli_close($conn);
    return FALSE;
  }

  // Insert new booking
  // Race condition: make sure nobody else booked the same machine between the SELECT and the INSERT query
  // $sql = "INSERT INTO BOOKING (MID, start, duration, ownerUID) VALUES ('$MID', '$starttime', '$duration', '$UID')";
  // Add a (complex) INSERT ... SELECT statement to make sure nobody else booked the same machine in the same time, then check if the row was actually inserted or not
  $sql = "INSERT INTO BOOKING (MID, start, duration, ownerUID)
    SELECT * FROM (SELECT '$MID' AS MID, '$starttime' AS starttime, '$duration' AS duration, '$UID' AS ownerUID) AS TMP
    WHERE NOT EXISTS (
      SELECT MID
      FROM BOOKING
      WHERE ADDTIME(start, SEC_TO_TIME(duration * 60)) > TIME('$starttime')
        AND start < ADDTIME(TIME('$starttime'), SEC_TO_TIME('$duration' * 60))
        AND MID = '$MID'
    )";
  if (!mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Errore durante l'inserimento della prenotazione: " . mysqli_error($conn);
    mysqli_close($conn);
    return FALSE;
  }
  if (mysqli_affected_rows($conn) === 0) {
    $msg = "Impossibile inserire la prenotazione al momento. Verifica la disponibilità e riprova.";
    mysqli_close($conn);
    return FALSE;
  }
  mysqli_close($conn);
  $msg = "Prenotazione inserita con successo.";
  return TRUE;
}


?>
