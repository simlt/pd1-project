<?php

require_once "conf.php";
require_once "password.php";

function dbConnect() {
  global $config;
  global $msg;
  $conn = mysqli_connect(
    $config["db"]["host"],
    $config["db"]["username"],
    $config["db"]["password"],
    $config["db"]["dbname"]
  );
  if (mysqli_connect_error()) {
    $msg = "Errore di collegamento al DB.";
    return;
  }
  return $conn;
}

function redirect($where) {
  header("Location: $where");
  exit;
}

function checkHTTPS() {
  return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
}

function forceHTTPS() {
  if (!checkHTTPS()) {
    // Force a new session if user was on http
    logout();
    if(!headers_sent()) {
      header("Status: 301 Moved Permanently");
      header(sprintf('Location: https://%s%s', $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']));
      exit;
    }
  }
}

function requireAuth() {
  session_start();
  forceHTTPS();
  // if not authenticated
  if (!isset($_SESSION["UID"]))
    redirect("login.php");
  // Check 2 minute inactivity before expiring session only if authenticated to avoid redirect loop
  checkAuthTimeout(TRUE);
}

// Returns TRUE if authenticated
function optionalAuth() {
  session_start();
  checkAuthTimeout();
  // check if authenticated only after the timeout check
  if (!isset($_SESSION["UID"]))
    return FALSE;
  // if user is authenticated check HTTPS
  if (!checkHTTPS()) {
    // Force a new session if user was on http
    logout();
    return FALSE;
  }
  return TRUE;
}

function checkAuthTimeout($redirect = FALSE) {
  if (!isset($_SESSION["UID"]))
    return;
  global $config;
  $last = $_SESSION["time"];
  if (time() - $last > $config["inactivityTimeout"]) {
    logout();
    if ($redirect) {
      redirect("login.php?msg=SessionTimeout");
    }
  }
  else {
    $_SESSION["time"] = time(); // Refresh session
  }
}

function initSession($UID, $name, $surname) {
  if (!isset ($_SESSION)) session_start();
  $_SESSION["UID"] = $UID;
  $_SESSION["time"] = time();
  $_SESSION["name"] = $name;
  $_SESSION["surname"] = $surname;
}

function getUserName() {
  // TODO this should be queried from DB in a function and not stored in session
  return $_SESSION["name"] . " " . $_SESSION["surname"];
}

function logout() {
  if (!isset ($_SESSION)) session_start();
  // Unset all of the session variables.
  $_SESSION = array();
  if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', 0, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
  }
  session_destroy();
}

function login($email, $password) {
  global $msg;
  if (!$conn = dbConnect())
    return;
  // Validation
  $utente = mysqli_real_escape_string($conn, $email);
  $sql = "SELECT UID, name, surname, hash FROM USER WHERE email = '$email'";
  if (!$res = mysqli_query($conn, $sql)) {
    $msg = "Query non valida: ". mysqli_error($conn);
    mysqli_close($conn);
  }
  $row = mysqli_fetch_assoc($res);
  if ($row && password_verify($password, $row["hash"])) {
    initSession($row["UID"], $row["name"], $row["surname"]);
    redirect("user.php");
  } else {
    $msg = "Utente o password errata.";
  }
  mysqli_free_result($res);
  mysqli_close($conn);
}

function register($name, $surname, $email, $password) {
  global $msg;
  // Validation
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $msg = "Email non valida.";
    return;
  }
  if (!$conn = dbConnect())
    return;
  $email = mysqli_real_escape_string($conn, $email);
  $sql = "SELECT * FROM USER WHERE email = '$email'";
  if (!$res = mysqli_query($conn, $sql)) {
    // TODO remove for production
    $msg = "Query non valida: " . mysqli_error($conn);
    mysqli_close($conn);
    return;
  }
  if (mysqli_num_rows($res) != 0) {
    $msg = "Email già registrata.";
    mysqli_close($conn);
    return;
  }
  // Validation (also filter strings to prevent XSS)
  $name = filter_var($name, FILTER_SANITIZE_STRING);
  $surname = filter_var($surname, FILTER_SANITIZE_STRING);
  $name = mysqli_real_escape_string($conn, $name);
  $surname = mysqli_real_escape_string($conn, $surname);
  $hash = password_hash($password, PASSWORD_DEFAULT);
  $sql = "INSERT INTO USER (name, surname, email, hash) VALUES ('$name', '$surname', '$email', '$hash')";
  if (mysqli_query($conn, $sql) === TRUE) {
    // Success
    mysqli_close($conn);
    login($email, $password);
  } else {
    // TODO remove for production
    $msg = "Errore nella registrazione: " . mysqli_error($conn);
  }
  mysqli_close($conn);
}


function showMsg($success = FALSE) {
  global $msg;
  if (isset($msg)) {
    $msg = htmlspecialchars($msg);
    if ($success)
      echo "<div class=\"alert alert-success\">
      <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
    else
      echo "<div class=\"alert alert-danger\">
      <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
    echo "<strong>$msg</strong>
    </div>";
  }
}


?>
