<?php

require "conf.php";
require LIB_PATH . "/functions.php";
require LIB_PATH . "/booking.php";

$isauth = optionalAuth();

if ($isauth)
  $user = getUserName();

$table = getBookingTable();

// Keep at end to be able to create redirect header before
$title = "Prenotazione macchina - Home";
include_once TEMPLATE_PATH . "/header.php";

?>

<!-- Body contents -->
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
  <?php showMsg(); ?>
  <h1 class="page-header">Elenco prenotazioni</h1>
  <?= $table ?>
</div>

<?php include_once TEMPLATE_PATH . "/footer.php"; ?>
