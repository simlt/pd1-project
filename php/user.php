<?php

require "conf.php";
require LIB_PATH . "/functions.php";
require LIB_PATH . "/booking.php";

requireAuth();

$user = getUserName();
$success = FALSE;

// Update booking before getting table
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["remove"])) {
    $success = removeBookingsForUser($_SESSION["UID"], $_POST["remove"]);
  }
  else
    $msg = "Selezionare almeno una prenotazione da rimuovere.";
}

// Get table only after an eventual update
$table = getBookingTableForUser($_SESSION["UID"]);

// Keep at end to be able to create redirect header before
$title = "Prenotazione macchina - Utente";
include_once TEMPLATE_PATH . "/header.php";

?>

<!-- Body contents -->
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
  <h3>Benvenuto <?= htmlspecialchars($user) ?>.</h3><br>
  <?php showMsg($success); ?>
  <h1 class="page-header">Le mie prenotazioni</h1>
  <form action="user.php" method="post">
    <?= $table ?>
  </form>
</div>

<?php include_once TEMPLATE_PATH . "/footer.php"; ?>
