<?php

require "conf.php";
require LIB_PATH . "/functions.php";

$msg = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  logout(); // Make sure to destroy the session that may be invalid to be able to start a fresh session
  if (isset($_GET["msg"]) && is_string($_GET["msg"]))
  {
    if (strcmp($_GET["msg"], "noJS") === 0) {
      $msg = "È necessario abilitare JavaScript per l'utilizzo del sito!";
    }
    elseif (strcmp($_GET["msg"], "noCookies") === 0) {
      $msg = "È necessario abilitare i cookies per l'utilizzo del sito!";
    }
  }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1><?= htmlspecialchars($msg) ?></h1>
    <h2><a href=".">Torna alla home (Riprova)</a><h2>
  </body>
</html>
