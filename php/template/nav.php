<div id="sidebar" class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
      <li><a href=".">Home</a></li>
    <?php if (isset($_SESSION["UID"])) { ?>
      <li><a href="user.php">Le mie prenotazioni</a></li>
      <li><a href="book.php">Prenota</a></li>
      <li><a href="logout.php">Logout</a></li>
    <?php } else { ?>
      <li><a href="login.php">Login</a></li>
      <li><a href="register.php">Registrati</a></li>
    <?php } ?>
  </ul>
</div>
