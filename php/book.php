<?php

require "conf.php";
require LIB_PATH . "/booking.php";

requireAuth();

$user = getUserName();
$success = FALSE;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (!empty($_POST["time"]) && !empty($_POST["duration"])) {
    $success = addBookingForUser($_SESSION["UID"], $_POST["time"], $_POST["duration"]);
  }
  else
    $msg = "Tutti i campi devono essere riempiti.";
}

// Keep at end to be able to create redirect header before
$title = "Prenotazione macchina - Utente";
include_once TEMPLATE_PATH . "/header.php";

// Custom scripts
$scripts = "<script src=\"dist/bootstrap-clockpicker.min.js\"></script>
<script>$('.clockpicker').clockpicker({'default': 'now', donetext: 'OK'});</script>";

?>

<!-- Body contents -->
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
  <?php showMsg($success); ?>
  <h1 class="page-header">Prenota</h1>
  <form class="form-inline" action="book.php" method="post">
    <div class="form-group">
      <label for="time">Inizio</label>
      <div class="input-group clockpicker">
        <input type="text" class="form-control" id="time" name="time" required>
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-time"></span>
        </span>
      </div>
    </div>
    <div class="form-group">
      <label for="duration">Durata</label>
      <input type="text" class="form-control" id="duration" name="duration" required>
    </div>
  <button type="submit" class="btn btn-primary">Prenota</button>
  </form>
</div>

<?php include_once TEMPLATE_PATH . "/footer.php"; ?>
