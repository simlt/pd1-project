<?php

require "conf.php";
require LIB_PATH . "/functions.php";

forceHTTPS();

// Redirect already authenticated users
if (optionalAuth())
  redirect("user.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (!empty($_POST["email"]) && !empty($_POST["password"])) {
    login($_POST["email"], $_POST["password"]);
  }
  else
    $msg = "Tutti i campi devono essere riempiti.";
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
  logout(); // Make sure to destroy the session that may be invalid to be able to start a fresh session
  if (isset($_GET["msg"]) && is_string($_GET["msg"]) && strcmp($_GET["msg"], "SessionTimeout") === 0) {
    $msg = "Sessione scaduta. Esegui nuovamente il login.";
  }
}

// Keep at end to be able to create redirect header before
$title = "Prenotazione macchina - Login";
include_once TEMPLATE_PATH . "/header.php";

?>

<!-- Body contents -->
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
  <?php showMsg(); ?>
  <h1 class="page-header">Login</h1>
  <form action="login.php" method="POST">
    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
  </form>
</div>

<?php include_once TEMPLATE_PATH . "/footer.php"; ?>
