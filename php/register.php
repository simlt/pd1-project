<?php

require "conf.php";
require LIB_PATH . "/functions.php";

forceHTTPS();

$name = "";
$surname = "";
$email = "";
$password = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = isset($_POST["name"]) ? $_POST["name"] : "";
  $surname = isset($_POST["surname"]) ? $_POST["surname"] : "";
  $email = isset($_POST["email"]) ? $_POST["email"] : "";
  $password = isset($_POST["password"]) ? $_POST["password"] : "";

  if (!empty($_POST["name"]) && !empty($_POST["surname"]) && !empty($_POST["email"]) && !empty($_POST["password"])) {
    register($name, $surname, $email, $password);
  }
  else
    $msg = "Tutti i campi devono essere riempiti.";
}

// Keep at end to be able to create redirect header before
$title = "Prenotazione macchina - Registrazione";
include_once TEMPLATE_PATH . "/header.php";

?>

<!-- Body contents -->
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
  <?php showMsg(); ?>
  <h1 class="page-header">Registrazione</h1>
  <form action="register.php" method="POST">
    <div class="form-group">
      <label for="name">Nome</label>
      <input type="text" class="form-control" id ="name" name="name" placeholder="Nome" value="<?= htmlspecialchars($name) ?>" required>
    </div>
    <div class="form-group">
      <label for="surname">Cognome</label>
      <input type="text" class="form-control" id ="surname" name="surname" placeholder="Cognome" value="<?= htmlspecialchars($surname) ?>" required>
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id ="email" name="email" placeholder="Email" value="<?= htmlspecialchars($email) ?>" required>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>
    <button type="submit" class="btn btn-primary">Registra</button>
  </form>
</div>

<?php include_once TEMPLATE_PATH . "/footer.php"; ?>
